package response

import (
	"bitbucket.org/joe_ad87/util/logger"
	"bitbucket.org/joe_ad87/util/usererror"
	"errors"
	"github.com/gin-gonic/gin"
	"net/http"
)

const (
	SuccessCode         = "00"
	SuccessMessage      = "Success"
	DefaultErrorCode    = "XX"
	DefaultErrorMessage = "Something went wrong"
)

type Status struct {
	ResponseCode       string                      `json:"responseCode"`
	ResponseMessage    string                      `json:"responseMassage"`
	ValidationMessages []usererror.ValidationError `json:"validationMessages,omitempty"`
}

type GeneralResponse struct {
	RequestId string `json:"requestId"`
	Status
	Data interface{} `json:"data,omitempty"`
}

func JsonSuccess(c *gin.Context, data interface{}) {
	status := Status{
		ResponseCode:    SuccessCode,
		ResponseMessage: SuccessMessage,
	}
	JsonResponse(c, http.StatusOK, status, data)
}

func JsonError(c *gin.Context, err error) {
	var status Status
	var httpStatus int

	var userError *usererror.UserError
	if errors.As(err, &userError) {
		httpStatus = userError.ErrorType
		status = Status{
			ResponseCode:       userError.ErrorCode,
			ResponseMessage:    userError.ErrorMessage,
			ValidationMessages: userError.ValidationErrors,
		}
	} else {
		httpStatus = http.StatusInternalServerError
		status = Status{
			ResponseCode:    DefaultErrorCode,
			ResponseMessage: DefaultErrorMessage,
		}
	}

	JsonResponse(c, httpStatus, status, nil)
}

func JsonResponse(c *gin.Context, httpStatus int, status Status, data interface{}) {
	var requestId string
	trace, ok := c.Request.Context().Value(logger.TraceContextKey).(logger.TraceRequest)
	if ok {
		requestId = trace.Trace
	}

	resp := GeneralResponse{
		RequestId: requestId,
		Status:    status,
		Data:      data,
	}
	c.JSON(httpStatus, resp)
}
