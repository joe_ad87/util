package logger

import (
	"time"

	"github.com/gin-gonic/gin"
)

func RequestLogger() gin.HandlerFunc {
	return func(c *gin.Context) {
		url := c.Request.URL.Path
		method := c.Request.Method

		Ctx(c).Info().Msgf("start [%s][%s]", method, url)
		start := time.Now()
		c.Next()
		elapsed := time.Since(start)
		Ctx(c).Info().Msgf("%s to complete [%s][%s]", elapsed, method, url)
	}
}
