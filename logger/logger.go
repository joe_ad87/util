package logger

import (
	"os"
	"sync"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
)

var (
	once sync.Once
	log  *zerolog.Logger
)

const (
	AppNameLogKey     = "appName"
	ClientTraceLogKey = "clientTrace"
	TraceLogKey       = "trace"
	SpanLogKey        = "span"
	ParentLogKey      = "parent"
	UrlLogKey         = "url"
	MethodLogKey      = "method"
	UsernameLogKey    = "Username"
)

func Ctx(c *gin.Context) *zerolog.Logger {
	url := c.Request.URL.Path
	method := c.Request.Method

	logContext := Get().With().
		Str(MethodLogKey, method).
		Str(UrlLogKey, url)

	trace, ok := c.Request.Context().Value(TraceContextKey).(TraceRequest)
	if ok {
		logContext = logContext.Str(ClientTraceLogKey, trace.Client).
			Str(TraceLogKey, trace.Trace).
			Str(SpanLogKey, trace.Span).
			Str(ParentLogKey, trace.Parent)
	}

	Username, ok := c.Request.Context().Value("Username").(string)
	if ok {
		logContext = logContext.Str(UsernameLogKey, Username)
	}
	logger := logContext.Logger()

	return &logger
}

func Get() *zerolog.Logger {
	once.Do(func() {
		logger := zerolog.New(os.Stdout).With().Timestamp().Logger()

		log = &logger
	})
	return log
}

func AddAppName(val string) {
	logger := Get().With().Str(AppNameLogKey, val).Logger()
	log = &logger
}
