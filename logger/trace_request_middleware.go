package logger

import (
	"context"
	"github.com/gin-gonic/gin"
	"github.com/rs/xid"
)

type TraceRequest struct {
	Client string `header:"X-Client-Trace"`
	Trace  string `header:"X-Trace"`
	Parent string `header:"X-Span"`
	Span   string
}

func generateSpanId() string {
	guid := xid.New()
	return guid.String()
}

func TraceRequestGenerator() gin.HandlerFunc {
	return func(c *gin.Context) {
		t := TraceRequest{}
		if err := c.ShouldBindHeader(&t); err != nil {
			Get().Error().Msg("Fail To Bind Trace Header")
		}

		t.Span = generateSpanId()
		if t.Trace == "" {
			t.Trace = t.Span
		}

		ctx := context.WithValue(c.Request.Context(), TraceContextKey, t)
		c.Request = c.Request.WithContext(ctx)

		c.Next()
	}
}
