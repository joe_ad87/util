package logger

const (
	TraceContextKey = "TRACE_CONTEXT"

	ClientHeader = "X-Client-Trace"
	TraceHeader  = "X-Trace"
	SpanHeader   = "X-Span"
)
