package hash

import (
	"crypto/md5"
	"encoding/hex"
)

type Hash interface {
	Compare(val string, hashVal string) bool
	Generate(val string) string
}

type md5Hash struct {
	salt string
}

func (m *md5Hash) Generate(val string) string {
	hash := md5.Sum([]byte(m.salt + val))
	return hex.EncodeToString(hash[:])
}

func (m *md5Hash) Compare(val string, hashVal string) bool {
	hash := m.Generate(val)
	if hash == hashVal {
		return true
	}
	return false
}

func NewMD5Hash(salt string) Hash {
	return &md5Hash{
		salt: salt,
	}
}
