package usererror

import (
	"net/http"
	"strconv"
)

const (
	ValidationErrorCode    = "VX"
	ValidationErrorMessage = "Invalid data"
)

var _ error = &UserError{}

type ValidationError struct {
	ErrorCode    string
	ErrorMessage string
	ErrorField   string
}

type UserError struct {
	ErrorCode        string
	ErrorMessage     string
	ErrorType        int
	ValidationErrors []ValidationError
}

func (u *UserError) Error() string {
	return u.ErrorCode + " - " + u.ErrorMessage
}

func DefaultValidationUserError(validationErrors []ValidationError) *UserError {
	return &UserError{
		ErrorCode:        ValidationErrorCode,
		ErrorMessage:     ValidationErrorMessage,
		ErrorType:        http.StatusBadRequest,
		ValidationErrors: validationErrors,
	}
}

func NewUserError(errorCode string, errorMsg string) *UserError {
	return &UserError{
		ErrorCode:    errorCode,
		ErrorMessage: errorMsg,
		ErrorType:    http.StatusBadRequest,
	}
}

func NewUnauthorizedUserError(errorCode string, errorMsg string) *UserError {
	return &UserError{
		ErrorCode:    errorCode,
		ErrorMessage: errorMsg,
		ErrorType:    http.StatusUnauthorized,
	}
}

func DefaultUnauthorizedUserError() *UserError {
	return &UserError{
		ErrorCode:    strconv.Itoa(http.StatusUnauthorized),
		ErrorMessage: http.StatusText(http.StatusUnauthorized),
		ErrorType:    http.StatusUnauthorized,
	}
}

func NewForbiddenUserError(errorCode string, errorMsg string) *UserError {
	return &UserError{
		ErrorCode:    errorCode,
		ErrorMessage: errorMsg,
		ErrorType:    http.StatusForbidden,
	}
}

func DefaultForbiddenUserError() *UserError {
	return &UserError{
		ErrorCode:    strconv.Itoa(http.StatusForbidden),
		ErrorMessage: http.StatusText(http.StatusForbidden),
		ErrorType:    http.StatusForbidden,
	}
}
